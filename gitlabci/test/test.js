// node.js default assertiion library
var assert = require('assert');
const Orc = require("../app.js");

// Outer main test group
describe('Array', function() {
    // Name of test group, callback function
    describe('#indexOf', function(){
        // Can nest more describes here...
        // It -> Individual test kit
        it('should return -1 when the value is not present', function(){
            assert.equal(-1, [1,2,3].indexOf(4));
        });
    });
    describe('Math tests', function(){
        it('should return -1 when the value is not present', function(){
            assert.equal(9, 3*3);
        });
        it('should return -1 when the value is not present', function(){
            assert.equal(-8, (3-4)*8);
        });
    });
    describe('Temperature Conversion', function(){
        it('ctof', function(){
            assert.equal(-40, Orc.cToF(-40));
        });
        it('ftoc', function(){
            assert.equal(-8, (3-4)*8);
        });
    });
});